import 'dart:html';
import 'dart:web_gl';

getShader(RenderingContext gl, String id) {
    var shaderScript = querySelector('#${id}');
  
    String str = "";
    for(var n in shaderScript.nodes){
      if(n.nodeType == 3){
        str += n.text;
      }
    }
    //print(str);
  
    var shader;
    if (shaderScript.type == "x-shader/x-fragment") {
        shader = gl.createShader(RenderingContext.FRAGMENT_SHADER);
    } else if (shaderScript.type == "x-shader/x-vertex") {
        shader = gl.createShader(RenderingContext.VERTEX_SHADER);
    } else {
        return null;
    }
  
    gl.shaderSource(shader, str);
    gl.compileShader(shader);
  
    if (!gl.getShaderParameter(shader, RenderingContext.COMPILE_STATUS)) {
        window.alert(gl.getShaderInfoLog(shader));
        return null;
    }
  
    return shader;
}

