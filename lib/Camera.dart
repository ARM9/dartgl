import 'package:vector_math/vector_math.dart';
import 'Entity3D.dart';

class Camera extends Entity3D{
  Matrix4 _pMatrix;
  num _fov, _aspect, _near, _far;
  
  Matrix4 get pMatrix => _pMatrix;
  
  Camera(this._fov, this._aspect, this._near, this._far){
    _pMatrix = makePerspectiveMatrix(radians(_fov), _aspect, _near, _far);
  }
  
  void update(){
    _pMatrix = makePerspectiveMatrix(radians(_fov), _aspect, _near, _far);
  }
}
