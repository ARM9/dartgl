import 'dart:collection';
import 'dart:html';
import 'dart:math' as Math;
import 'dart:typed_data';
import 'dart:web_gl';

import 'package:vector_math/vector_math.dart';
import 'BumboEngine.dart';
import 'Camera.dart';

class Renderer {
  var gl;
  
  CanvasElement _canvas;
  Shader _vs;
  Shader _fs;
  var _gouraudShader;
  
  Buffer _triangleVertexPosBuffer, _triangleVertexColorBuffer;
  Buffer _squareVertexPosBuffer, _squareVertexColorBuffer;
  
  int _aVertexPosition, _aVertexColor;
  
  var _mvMatrix;
  UniformLocation _uPMatrix;
  UniformLocation _uMVMatrix;
  
  Queue<Matrix4> _matrixStack;
  
  Camera _camera;
  
  Renderer(width, height){
    _canvas = new CanvasElement();
    _canvas.width = width;
    _canvas.height = height;
    _canvas.style..width='$width px'..height='$height px'..backgroundColor='#1385bb';
    _initGL();
  }
  
  void _initGL(){
    try {
      gl = _canvas.getContext3d();//getContext("experimental-webgl");
      gl.viewport(0, 0, _canvas.width, _canvas.height);
      
      _canvas.addEventListener("webglcontextlost", (event) => event.preventDefault(), false);
      _canvas.addEventListener("webglcontextrestored", (event) => _initGL(), false);
    } catch (err) {
      window.alert('Error: ${err}');
    }
    
    _matrixStack = new Queue();
    initShaders();
    initBuffers();
  }
  
  void initShaders(){
    _vs = getShader(gl, "gouraud-vs");
    _fs = getShader(gl, "gouraud-fs");
    
    _gouraudShader = gl.createProgram();
    gl.attachShader(_gouraudShader, _vs);
    gl.attachShader(_gouraudShader, _fs);
    try{
      gl.linkProgram(_gouraudShader);
      gl.useProgram(_gouraudShader);
    }catch(err){
      window.alert('Could not initialise shaders: ${err}');
    }
    if (!gl.getShaderParameter(_vs, RenderingContext.COMPILE_STATUS)) {
      print(gl.getShaderInfoLog(_vs));
    }
    if (!gl.getShaderParameter(_fs, RenderingContext.COMPILE_STATUS)) {
      print(gl.getShaderInfoLog(_fs));
    }
    if (!gl.getProgramParameter(_gouraudShader, RenderingContext.LINK_STATUS)) {
      print(gl.getProgramInfoLog(_gouraudShader));
    }
    
    _aVertexPosition = gl.getAttribLocation(_gouraudShader, "aVertexPosition");
    gl.enableVertexAttribArray(_aVertexPosition);
    _aVertexColor = gl.getAttribLocation(_gouraudShader, "aVertexColor");
    gl.enableVertexAttribArray(_aVertexColor);
      
    _uPMatrix = gl.getUniformLocation(_gouraudShader, "uPMatrix");
    _uMVMatrix = gl.getUniformLocation(_gouraudShader, "uMVMatrix");
  }
  
  void initBuffers(){
    List<double> vertices = [
          0.0, 1.0, 0.0,
          -1.0, -1.0, 0.0,
          1.0, -1.0, 0.0];
    
    _triangleVertexPosBuffer = gl.createBuffer();
    gl.bindBuffer(RenderingContext.ARRAY_BUFFER, _triangleVertexPosBuffer);
    gl.bufferDataTyped(RenderingContext.ARRAY_BUFFER, new Float32List.fromList(vertices), RenderingContext.STATIC_DRAW);
    
    var colors = new List<double>(12);
    for(int i = 0; i < colors.length; i++){
      colors[i] = 1.0/i;
    }
    _triangleVertexColorBuffer = gl.createBuffer();
    gl.bindBuffer(RenderingContext.ARRAY_BUFFER, _triangleVertexColorBuffer);
    gl.bufferDataTyped(RenderingContext.ARRAY_BUFFER, new Float32List.fromList(colors), RenderingContext.STATIC_DRAW);
    
    vertices = [
          1.0, 1.0, 0.0,
          -1.0, 1.0, 0.0,
          1.0, -1.0, 0.0,
          -1.0, -1.0, 0.0];
    
    _squareVertexPosBuffer = gl.createBuffer();
    gl.bindBuffer(RenderingContext.ARRAY_BUFFER, _squareVertexPosBuffer);
    gl.bufferDataTyped(RenderingContext.ARRAY_BUFFER, new Float32List.fromList(vertices), RenderingContext.STATIC_DRAW);
  
    colors = new List<double>(16);
    for(int i = 0; i < colors.length; i++){
     colors[i] = Math.sin(i);
    }
    _squareVertexColorBuffer = gl.createBuffer();
    gl.bindBuffer(RenderingContext.ARRAY_BUFFER, _squareVertexColorBuffer);
    gl.bufferData(RenderingContext.ARRAY_BUFFER, new Float32List.fromList(colors), RenderingContext.STATIC_DRAW);
  }
  
  void setMatrixUniforms(){
    Float32List t = new Float32List(16);
    _camera.pMatrix.copyIntoArray(t);
    gl.uniformMatrix4fv(_uPMatrix, false, t);
    _mvMatrix.copyIntoArray(t);
    gl.uniformMatrix4fv(_uMVMatrix, false, t);
  }
  
  void render(cam){
    _camera = cam;
   
    _renderFrame(0);
  }
  
  Matrix4 popMatrix(){
    if(_matrixStack.length < 1){
      print('Invalid popMatrix.');
      return null;
    }
    return _matrixStack.removeFirst();
  }
  
  void pushMatrix(Matrix4 m){
    _matrixStack.addFirst(m.clone());
  }
  
  double y = 0.0, z = 0.0;
  void _renderFrame(num dt){
    window.animationFrame.then(_renderFrame);
    
    gl.viewport(0, 0, _canvas.width, _canvas.height);
    gl.clear(RenderingContext.COLOR_BUFFER_BIT | RenderingContext.DEPTH_BUFFER_BIT);
    _camera.update();
    
    _mvMatrix = new Matrix4.identity();
    
    _mvMatrix.translate(1.5, 0.0, -7.0);
    pushMatrix(_mvMatrix);
    _mvMatrix.rotate(new Vector3(0.0, 1.0, 0.0), y+=0.01);
    
    gl.useProgram(_gouraudShader);
    gl.bindBuffer(RenderingContext.ARRAY_BUFFER, _triangleVertexPosBuffer);
    gl.vertexAttribPointer(_aVertexPosition, 3, RenderingContext.FLOAT, false, 0, 0);
    gl.bindBuffer(RenderingContext.ARRAY_BUFFER, _triangleVertexColorBuffer);
    gl.vertexAttribPointer(_aVertexColor, 4, RenderingContext.FLOAT, false, 0, 0);
    setMatrixUniforms();
    gl.drawArrays(RenderingContext.TRIANGLES, 0, 3);
    
    _mvMatrix = popMatrix();
    _mvMatrix.translate(-3.0, 0.0, 0.0);
    _mvMatrix.rotate(new Vector3(0.0, 0.0, 1.00), z+=0.01);
    
    gl.bindBuffer(RenderingContext.ARRAY_BUFFER, _squareVertexPosBuffer);
    gl.vertexAttribPointer(_aVertexPosition, 3, RenderingContext.FLOAT, false, 0, 0);
    gl.bindBuffer(RenderingContext.ARRAY_BUFFER, _squareVertexColorBuffer);
    gl.vertexAttribPointer(_aVertexColor, 4, RenderingContext.FLOAT, false, 0, 0);
    
    setMatrixUniforms();
    
    gl.drawArrays(RenderingContext.TRIANGLE_STRIP, 0, 4);
  }
  
  CanvasElement getDOM() => _canvas;
}
