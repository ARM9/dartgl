import 'dart:html';
import 'lib/Renderer.dart';
import 'lib/Camera.dart';

void main() {
  int width = 640, height = 480;
  Renderer renderer = new Renderer(width, height);//window.innerWidth, window.innerHeight);
  querySelector('body').append(renderer.getDOM());
  
  Camera cam = new Camera(45.0, width/height, 1.0, 1000.0);
  renderer.render(cam);
}
